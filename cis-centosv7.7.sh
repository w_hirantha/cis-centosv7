#!/bin/bash
# script name cis-centosv7.7.sh
# version v1.0
# Author w.hiratha@mubasher.net
# Copyright (c) Mubasher Financial Services
#
# Source function library.
. /etc/init.d/functions
# Check system as internet access else compain and exit.

baseDir=`pwd`
source ./functions.sh
# Check if running with root User
check_root() {
if [ $EUID -ne 0 ]; then
      echo "Permission Denied"
      echo "Can only be run by root"
      exit
else
      clear
      f_banner
      cat templates/texts/welcome-CIS
      say_continue
fi
}

check_root

AUDITDIR="/root/.$(hostname -s)_audit"
TIME="$(date +"%m%d%y-%H%m")"
if [ ! -d  $AUDITDIR ]; then mkdir -p $AUDITDIR
fi

if [[ $(curl -Is https://www.google.com | head -n 1 | awk '{print $2}') = 200 ]]; then
  echo "  Found Internet ;-)"
else
  echo "  Check your Internet ;-( "
  echo " "
  exit
fi

##############################################
clear
f_banner
 echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
        echo -e "\e[93m[+]\e[00m Preparing the server..."
        echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
        echo ""
echo "What do you call this server (hostname)?"
read myhostname
hostnamectl set-hostname $myhostname
echo "Server name changed to `hostname`"

echo "Install essential packages..."
# Package installation
echo "nameserver 8.8.8.8" > /etc/resolv.conf
yum install bind-utils ntpdate nmap-ncat vim-enhanced vim-common \
	net-tools wget curl lsof bash-completion screen git rsyslog epel-release.noarch -y

#1.2 Configure Software Updates
yum upgrade -y

# Set nproc
echo "Setting up number of processors per user to 60,000.."
sleep 1s
echo "*        -    nproc          60000" > /etc/security/limits.d/20-nproc.conf || failure "Something's wrong while setting up nproc"


echo " "
# 2.2.1.1 Ensure time synchronization is in use (Not Scored)
# 2.2.1.2 Ensure ntp is configured (Scored)
echo "2.2.1.2 Ensure ntp is configured (Scored)"
echo " Setting up time zone (UTC) and the time"
/bin/ln -sf /usr/share/zoneinfo/UTC /etc/localtime || failure "Something's wrong while setting up UTC"

# 2.2.1.3 Ensure chrony is configured (Scored)
clear
f_banner
 echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
        echo -e "\e[93m[+]\e[00m 2.2.1.3 Ensure chrony is configured (Scored)"
        echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
        echo ""
yum install chrony -y
cat $baseDir/templates/chrony.conf > /etc/chrony.conf
systemctl enable chronyd
systemctl start chronyd
chronyc sources
sleep 2s
chronyc sources -v
sleep 2s
chronyc tracking
sleep 2s
chronyc -a "burst 3/5"
sleep 2s
chronyc -a makestep

echo " Setting up hardware clock"
/sbin/hwclock -s

# set the bash profile
cp $baseDir/templates/cis-bash_profile.sh /etc/profile.d/
chmod 644 /etc/profile.d/cis-bash_profile.sh

#5.3.4 Ensure password hashing algorithm is SHA-512 (Scored)
/usr/sbin/authconfig --passalgo=sha512 --update
	
clear
f_banner
echo "1.1.1 Disable unused filesystems"

# 1.1.1 Disable unused filesystems
echo "Disabling Legacy Filesystems"
cat > /etc/modprobe.d/CIS.conf << "EOF"
install cramfs /bin/true
install freevxfs /bin/true
install jffs2 /bin/true
install hfs /bin/true
install hfsplus /bin/true
install squahfs /bin/true
install udf /bin/true
install dccp /bin/true
install sctp /bin/true
install rds /bin/true
install tipc /bin/true
install vfat /bin/true
EOF

clear
f_banner
# 1.1.14 Ensure nodev option set on /home partition (scored)
echo "1.1.14 Ensure nodev option set on /home partition"
echo " "
mount | grep /home | grep nodev >/dev/null
if [ $? -ne 0 ]; then
        echo "  /home partion not found or nodev not set"
        say_continue
else
        echo "not implemented.."
fi

# 1.1.15 Ensure nodev option set on /dev/shm partition (Scored)
# 1.1.16 Ensure nosuid option set on /dev/shm partition (Scored)
# 1.1.17 Ensure noexec option set on /dev/shm partition (Scored)
#1.4.1 Ensure permissions on bootloader config are configured (Scored)

clear
f_banner
 echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
        echo -e "\e[93m[+]\e[00m 1.3.1 Ensure AIDE is installed"
        echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
        echo ""
#1.3 Filesystem Integrity Checking
#1.3.1 Ensure AIDE is installed

yum install aide -y
spinner
/usr/sbin/aide --init && cp /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz
spinner
/usr/sbin/aide --check

# 1.3.2 Ensure filesystem integrity is regularly checked
# Configure periodic execution of AIDE, runs every morning at 04:30 UTC
echo "30 4 * * * root /usr/sbin/aide --check" >> /etc/crontab
say_done

#4 Logging and Auditing
#4.1 Configure System Accounting (auditd)
clear
f_banner
echo "4.1 Configure System Accounting (auditd)"

mv /etc/audit/auditd.conf ${AUDITDIR}/auditd.conf.$TIME
cp $baseDir/templates/auditd-CIS.conf /etc/audit/auditd.conf
systemctl enable auditd

# 1.5 Additional Process Hardening
clear
f_banner
echo "1.5 Additional Process Hardening"
echo " "
#1.5 Additional Process Hardening
# Set Kernel parameters to prevent certain kinds of attacks
tprint=`/sbin/sysctl -n net.ipv4.tcp_timestamps` > /dev/null 2>&1
if [ $tprint -ne 1 ]; then
        echo "  Kenerl Parameter setup is skipped, maybe it is done"
        say_continue
else
        echo " Setting up Kernel parameters"
        echo "#-----Set Kernel Parameters-----#" >> ${AUDITDIR}/postinstall.log
        cp /etc/sysctl.conf ${AUDITDIR}/sysctl.conf.$TIME
        cp $baseDir/templates/sysctl-CIS.conf /etc/sysctl.d/
        /sbin/sysctl -q -p /etc/sysctl.d/sysctl-CIS.conf
        spinner
        sleep 3s
fi

# 2 Services
clear
f_banner
echo "2 Services"
echo " "

# Stop unwanted services and start wanted ones.
echo "Disabling Unnecessary Services..."
servicelist=(dhcpd avahi-daemon cups nfslock rpcgssd rpcbind rpcidmapd firewalld rpcsvcgssd)
for i in ${servicelist[@]}; do
  [ $(systemctl disable $i 2> /dev/null) ] || echo "$i is Disabled"
done
sleep 3s

echo "Enabling necessary Services..."
servicelist=(rsyslog auditd)
for i in {servicelist[@]}; do
	[ $(systemctl enable $i 2> /dev/null)] || echo "$i is enabled"
	[ $(systemctl start $i 2> /dev/null)] || echo "$i is started"
done
sleep 3s

#4.1.3 Ensure auditing for processes that start prior to auditd is enabled (Scored)
sed -i.org 's/\<quiet\>/audit=1 &/' /etc/default/grub
grub2-mkconfig -o /boot/grub2/grub.cfg

cp $baseDir/templates/audit-CIS.rules /etc/audit/audit.rules

find / -xdev \( -perm -4000 -o -perm -2000 \) -type f | awk '{print \
"-a always,exit -F path=" $1 " -F perm=x -F auid>=1000 -F auid!=4294967295 \
-k privileged" } ' >> /etc/audit/audit.rules

echo " " >> /etc/audit/audit.rules
echo "#End of Audit Rules" >> /etc/audit/audit.rules
echo "-e 2" >>/etc/audit/audit.rules

cp /etc/audit/audit.rules /etc/audit/rules.d/audit.rules


#4.2.4 Ensure permissions on all logfiles are configured (Scored)
chmod -R g-wx,o-rwx /var/log/*

#5.3 Configure PAM
#5.3.1 Ensure password creation requirements are configured (Scored)
#5.3.2 Ensure lockout for failed password attempts is configured (Not Scored)
#5.3.3 Ensure password reuse is limited (Scored)
#5.3.4 Ensure password hashing algorithm is SHA-512 (Scored)
grep remember=5 /etc/pam.d/system-auth >/dev/null
 if [ $? -ne 0 ]; then
        mv /etc/security/pwquality.conf ${AUDITDIR}/pwquality.conf.$TIME
        cp $baseDir/templates/pwquality-CIS.conf /etc/security/pwquality.conf
        sed -i.org 's/\use_authtok\>/remember=5 &/' /etc/pam.d/password-auth
        sed -i.org 's/\use_authtok\>/remember=5 &/' /etc/pam.d/system-auth
else
        echo " #5.3 Configure PAM is skpped, maybe it is done..."
        sleep 2s
fi

#5.1.2 Ensure permissions on /etc/cron* are configured (Scored)
chown root:root /etc/cron*
chmod og-rwx /etc/cron*


# Deny All TCP Wrappers except SSH
echo "ALL:ALL" >> /etc/hosts.deny
echo "sshd:ALL" >> /etc/hosts.allow

#5.2 SSH Server Configuration
checkSsh=`egrep "^(PermitRootLogin|#PermitRootLogin)" /etc/ssh/sshd_config | awk '{print $1}'`

if [ ${checkSsh} != '#PermitRootLogin' ]; then
	echo "5.2 SSH Server Configuration skipped.."
    say_continue
else
	mv /etc/ssh/sshd_config ${AUDITDIR}/sshd_config.$TIME
	cp $baseDir/templates/sshd_config-CIS /etc/ssh/sshd_config
	
	##Create user for SSH Access
	echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
	echo -e "\e[93m[+]\e[00m We will now Create a New User for SSH Access"
	echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
	echo ""
	read -p "Enter username : " username
	getent passwd $username > /dev/null
	#grep "^$username" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo " You have added an existing user - $username"
    	sed -i s/USERNAME/$username/g /etc/ssh/sshd_config
    	if [[ -z "$username" ]]; then
    		printf '%s\n' "No input entered"
        	echo "WARNNING! User not added; The system might locked on next boot."
        	sleep 3s
    	fi
	else
		read -p "Enter password : " password
		pass=$(openssl passwd -crypt $password)
    	useradd -m -p $pass $username
    	sed -i s/USERNAME/$username/g /etc/ssh/sshd_config
    	sleep 2s
	fi
fi


        
# 5.2.1 Ensure permissions on /etc/ssh/sshd_config are configured
chown root:root /etc/ssh/sshd_config
chmod og-rwx /etc/ssh/sshd_config

#5.4.2 Ensure system accounts are non-login (Scored)

for user in `awk -F: '($3 < 1000) {print $1 }' /etc/passwd`; do
  if [ $user != "root" ]; then
    usermod -L $user
  if [ $user != "sync" ] && [ $user != "shutdown" ] && [ $user != "halt" ]; then
    usermod -s /usr/sbin/nologin $user
  fi
  fi
done

#5.4.3 Ensure default group for the root account is GID 0 (Scored)

usermod -g 0 root

#5.4.4 Ensure default user umask is 027 or more restrictive (Scored)


#5.5 Ensure root login is restricted to system console (Not Scored)
#5.6 Ensure access to the su command is restricted (Scored)

#6 System Maintenance
#6.1 System File Permissions
#6.1.1 Audit system file permissions (Not Scored)
#6.1.2 Ensure permissions on /etc/passwd are configured (Scored)
clear
f_banner
echo -e ""
echo -e "Setting System File Permissions"
spinner
sleep 2


chown root:root /etc/passwd
chmod 644 /etc/passwd

#6.1.3 Ensure permissions on /etc/shadow are configured (Scored)

chown root:root /etc/shadow
chmod o-rwx,g-wx /etc/shadow

#6.1.4 Ensure permissions on /etc/group are configured (Scored)

chown root:root /etc/group
chmod 644 /etc/group

#6.1.5 Ensure permissions on /etc/gshadow are configured (Scored)

chown root:root /etc/gshadow
chmod 000 /etc/gshadow

#6.1.6 Ensure permissions on /etc/passwd - are configured (Scored)

chown root:root /etc/passwd-
chmod u-x,go-wx /etc/passwd-

#6.1.7 Ensure permissions on /etc/shadow - are configured (Scored)

chown root:root /etc/shadow-
chmod 000 /etc/shadow-

#6.1.8 Ensure permissions on /etc/group - are configured (Scored)

chown root:root /etc/group-
chmod u-x,go-wx /etc/group-

#6.1.9 Ensure permissions on /etc/gshadow - are configured (Scored)

chown root:root /etc/gshadow-
chmod 000 /etc/gshadow-



# SELINUX Disable
checkSelinux=`grep ^SELINUX= /etc/selinux/config | sed 's/SELINUX=//'`

if [ ${checkSelinux} = 'disabled' ]; then
        echo "SELinux configure skpped, maybe it is done..."
        sleep 2s
else
    echo "SELinux disabling..."
    echo "#-----Backing up selinux config file-----#" >> ${AUDITDIR}/postinstall.log
    cp /etc/selinux/config $AUDITDIR/selinux_config.$TIME
    sed -i.org s/SELINUX=enforcing/SELINUX=disabled/g /etc/selinux/config
    #cat /etc/selinux/config | sed s/SELINUX=enforcing/SELINUX=disabled/ > $AUDITDIR/selinux.post || failure "Something's wrong in selinux"
    #mv -f $AUDITDIR/selinux.post /etc/selinux/config && success || failure "Something's wrong in selinux"
    echo "#-----SELinux modified. Reboot required.-----#"
    sleep 3s
fi

# 6.2.15 Ensure all groups in /etc/passwd exist in /etc/group (Scored)
for i in $(cut -s -d: -f4 /etc/passwd | sort -u );
do grep -q -P "^.*?:[^:]*:$i:" /etc/group
	if [ $? -ne 0 ]; then
		echo "Group $i is referenced by /etc/passwd but does not exist in /etc/group"
	fi
done
sleep 3s

clear
f_banner
echo "1.7 Warning Banners"
# 1.7 Warning Banners

sed -i "s/\#Banner none/Banner \/etc\/issue\.net/" /etc/ssh/sshd_config
cp -p /etc/issue.net ${AUDITDIR}/issue.net.$TIME
cat > /etc/issue.net << "EOF"
################################################################################

              All connections are monitored and recorded
           Unauthorized access to this server is prohibited
 Any intrusion attempts will be reported to all Law Enforcement Agencies
  Avoid Legal Charges, Disconnect NOW if you're not an authorized user!

################################################################################
EOF

cp -p /etc/motd ${AUDITDIR}/motd_$TIME
echo -en "\033[0;31m" > /etc/motd
cat << EOF >> /etc/motd

        /------------------------------------------------------------------------\     
        |                                                                        |
        |                        *** NOTICE TO USERS ***                         |
        |                                                                        |
        | This computer system is the private property of MUBASHER FINANCIAL     |
        | SERVICES. It is for authorized use only.                               |
        |                                                                        |
        | Users (authorized or unauthorized) have no explicit or implicit        |
        | expectation of privacy.                                                |
        |                                                                        |
        | Any or all uses of this system and all files on this system may be     |
        | intercepted, monitored, recorded, copied, audited, inspected, and      |
        | disclosed to your employer, to authorized site, government, and law    |
        | enforcement personnel, as well as authorized officials of government   |
        | agencies, both domestic and foreign.                                   |
        |                                                                        |
        | By using this system, the user consents to such interception,          |
        | monitoring, recording, copying, auditing, inspection, and disclosure   |
        | at the discretion of such personnel or officials.  Unauthorized or     |
        | improper use of this system may result in civil and criminal penalties |
        | and administrative or disciplinary action, as appropriate. By          |
        | continuing to use this system you indicate your awareness of and       |
        | consent to these terms and conditions of use. LOG OFF IMMEDIATELY if   |
        | you do not agree to the conditions stated in this warning.             |
        \------------------------------------------------------------------------/

EOF
echo -en "\033[0m" >> /etc/motd

#1.7.1.4 Ensure permissions on /etc/motd are configured (Not Scored)
#1.7.1.5 Ensure permissions on /etc/issue are configured (Scored)
#1.7.1.6 Ensure permissions on /etc/issue.net are configured (Not Scored)
chown root:root /etc/motd /etc/issue /etc/issue.net
chmod 644 /etc/motd /etc/issue /etc/issue.net
say_done