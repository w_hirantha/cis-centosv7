#!/bin/bash
#############################################################
#  MFS Dedicated server specific root aliases and functions
#  written and managed by Hirantha W.  (w.hirantha@mubasher.net)
#############################################################

#################################################
# interactive failsafes and general aliasing
#################################################
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias grep='grep --color'

############################
# Important history hacks
############################
#how many lines to keep in the history FILE
export HISTFILESIZE=500000
#how many lines to keep in memory for this SESSION.
export HISTSIZE=200000
#how to make it oh so very pretty.
HISTTIMEFORMAT="(%m/%d/%y) %T "
export HISTTIMEFORMAT
#append the history for all terminals
shopt -s histappend
export PROMPT_COMMAND='history -a'
# Auto exist the unix shell when no activity for 15 minutes
TMOUT=900

###########################
# give some terminal <3
###########################
#set check window resize
shopt -s checkwinsize

#######################
# vim defaults
#######################
alias vi=vim
export EDITOR=vim
