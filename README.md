# CIS Benchmark for CentOS v7
### The program is still under development hence use it at your own risk. Tested on CentOS v7.7 and above. You must have Internet to the targeted server.

### You must have git client and curl installed before running the program.

```
yum install git curl -y
```
###  You must then clone the program and execute as below.
```
git clone https://w_hirantha@bitbucket.org/w_hirantha/cis-centosv7.git
cd cis-centosv7
sh cis-centosv7.7.sh

```